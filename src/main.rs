use anyhow::{bail, Result};
use serde::Serialize;
use std::ffi::OsStr;
use std::fs::{read_to_string, File};
use std::io::{stdout, BufReader, Write};
use std::path::{Path, PathBuf};
use structopt::StructOpt;
use tera::{Context, Tera};

/// Render tera templates based on structured JSON data.
#[derive(Debug, StructOpt)]
struct Args {
    /// Template file in tera format.
    #[structopt(name = "TEMPLATEFILE")]
    templatefile: PathBuf,

    /// Structured data file in JSON format.
    #[structopt(name = "DATAFILE(S)", required = true)]
    datafile: Vec<PathBuf>,

    /// The output file path.
    /// If this is not given, the result will be written to stdout.
    #[structopt(short, long, name = "OUTPUTFILE")]
    output: Option<PathBuf>,
}

#[derive(Serialize)]
#[serde(untagged)]
enum DataValue {
    Json(serde_json::Value),
    Yaml(serde_yaml::Value),
    Toml(toml::Value),
}

trait WrappableIfNeeded {
    fn wrapped_if_needed(self) -> Self;
}

impl WrappableIfNeeded for DataValue {
    fn wrapped_if_needed(self) -> Self {
        match self {
            DataValue::Json(v) => DataValue::Json(v.wrapped_if_needed()),
            DataValue::Yaml(v) => DataValue::Yaml(v.wrapped_if_needed()),
            DataValue::Toml(v) => DataValue::Toml(v.wrapped_if_needed()),
        }
    }
}

impl WrappableIfNeeded for serde_json::Value {
    fn wrapped_if_needed(self) -> Self {
        use serde_json::{json, Value};
        match self {
            Value::Null
            | Value::Bool(_)
            | Value::Number(_)
            | Value::String(_)
            | Value::Array(_) => json!({ "data": self }),
            Value::Object(_) => self,
        }
    }
}

impl WrappableIfNeeded for serde_yaml::Value {
    fn wrapped_if_needed(self) -> Self {
        use serde_yaml::{Mapping, Value};
        match self {
            Value::Null
            | Value::Bool(_)
            | Value::Number(_)
            | Value::String(_)
            | Value::Sequence(_) => {
                let mut mapping = Mapping::with_capacity(1);
                mapping.insert(Value::String("data".into()), self);
                Value::Mapping(mapping)
            }
            Value::Mapping(_) => self,
        }
    }
}

impl WrappableIfNeeded for toml::Value {
    fn wrapped_if_needed(self) -> Self {
        use toml::value::{Table, Value};
        match self {
            Value::Boolean(_)
            | Value::Integer(_)
            | Value::Float(_)
            | Value::Datetime(_)
            | Value::Array(_)
            | Value::String(_) => {
                let mut table = Table::with_capacity(1);
                table.insert("data".into(), self);
                Value::Table(table)
            }
            Value::Table(_) => self,
        }
    }
}

fn load_json(path: &Path) -> Result<serde_json::Value> {
    use serde_json::{from_reader, Value};
    let json: Value = from_reader(BufReader::new(File::open(path)?))?;
    Ok(json)
}

fn load_yaml(path: &Path) -> Result<serde_yaml::Value> {
    use serde_yaml::{from_reader, Value};
    let yaml: Value = from_reader(BufReader::new(File::open(path)?))?;
    Ok(yaml)
}

fn load_toml(path: &Path) -> Result<toml::Value> {
    use toml::{from_str, value::Table, Value};
    let s = read_to_string(path)?;
    let toml: Value = from_str(&s)?;
    Ok(match toml {
        Value::Boolean(_)
        | Value::Integer(_)
        | Value::Float(_)
        | Value::Datetime(_)
        | Value::Array(_)
        | Value::String(_) => {
            let mut table = Table::with_capacity(1);
            table.insert("data".into(), toml);
            Value::Table(table)
        }
        Value::Table(_) => toml,
    })
}

fn load_datafile(path: &Path) -> Result<DataValue> {
    match path.extension() {
        Some(e) if e == OsStr::new("json") => {
            Ok(DataValue::Json(load_json(path)?))
        }
        Some(e) if e == OsStr::new("yaml") => {
            Ok(DataValue::Yaml(load_yaml(path)?))
        }
        Some(e) if e == OsStr::new("toml") => {
            Ok(DataValue::Toml(load_toml(path)?))
        }
        Some(e) => bail!(
            "Couldn't determine data file type (unknown extension {:?})",
            e
        ),
        None => {
            bail!("Couldn't determine data file type (no extension found)")
        }
    }
}

fn generate_completions(output_dir: &Path) -> Result<()> {
    println!("Generating shell completions");
    if !output_dir.is_dir() {
        bail!(
            "The shell completions output path {:?} \
                    is not a directory",
            output_dir
        );
    }
    use std::str::FromStr;
    use structopt::clap::Shell;
    for shell in Shell::variants().iter() {
        Args::clap().gen_completions(
            env!("CARGO_PKG_NAME"),
            Shell::from_str(shell).unwrap(),
            &output_dir,
        );
    }
    Ok(())
}

fn main() -> Result<()> {
    if let Ok(ev) = std::env::var("TERENDER_GENERATE_SHELL_COMPLETIONS") {
        let output_dir = Path::new(&ev);
        return generate_completions(output_dir);
    }

    let args = Args::from_args();

    let template = read_to_string(&args.templatefile)?;

    let datafiles = args
        .datafile
        .iter()
        .map(|p| load_datafile(p))
        .collect::<Result<Vec<_>>>()?;
    let context = match datafiles.len() {
        0 => unreachable!("Having no datafiles is impossible, because structopt prevents it"),
        1 => {
            Context::from_serialize(datafiles.into_iter().map(|d|d.wrapped_if_needed()).next())?
        }
        _ => {
            let mut context = Context::new();
            context.insert("data", &datafiles);
            context
        }
    };

    let stdout = stdout();

    let mut output: Box<dyn Write> = if let Some(p) = args.output {
        Box::new(File::create(&p)?)
    } else {
        Box::new(stdout.lock())
    };

    let rendered = Tera::one_off(&template, &context, false)?;
    write!(output, "{}", rendered)?;

    Ok(())
}
